'use strict';

const fs = require('fs');
const path = require('path');

const seedsData = require('../data/seeds');
const traysData = require('../data/trays');

async function addSeed(body){
    console.log('save seed in db.ctlr');
    if(!body.name)
        throw 'Name reqired';
    
    if(Object.values(seedsData).find(s => s.name === body.name))
        throw "Seed already exists";

    const id = Date.now().toString();
    body.id = id;

    seedsData[id] = body;

    fs.writeFile('./server/data/seeds.json', JSON.stringify(seedsData), err => {
        console.log('writefile err', err);
    });   
}

async function addTray(body){
    if(!body.name)
        throw 'Name reqired';
    if(!body.seed)
        throw 'Seed reqired';

    if(Object.values(traysData).find(t => t.name === body.name))
        throw 'Tray name in use';

    const id = Date.now().toString();
    body.id = id;
    body.startDate = Date.now();
    body.watered = body.startDate;

    traysData[id] = body;

    fs.writeFile('./server/data/trays.json', JSON.stringify(traysData), err => {
        console.log('writefile err', err);
    });
}

function deleteTray(id){
    if(!traysData[id]) return;  
    
    delete traysData[id];
    
    fs.writeFile('./server/data/trays.json', JSON.stringify(traysData), err => {
        console.log('writefile err', err);
    });
}

function getSeeds(){
    console.log('get seed in db.ctlr');
    return seedsData;
}

function getTasks(){
    let ary = [];
    Object.values(traysData).forEach(t => {
        // if last time watered was more than a day ago
        if(t.watered + (1000*60*60*24) < Date.now())
            ary.push({'type': 'water', trayId: t.id, name: t.name});
    });  
    
    return ary;
}

function getTrays(idsToKeep){
    const trays = JSON.parse( JSON.stringify(traysData) );
    // add seed data to seed property
    Object.values(trays).forEach(t => t.seed = seedsData[t.seed]);
    
    if(idsToKeep)
        trays = trays.filter(t => idsToKeep.indexOf(t.id) > -1);

    return trays;
}

async function updateTray(id, prop, value){
    let tray = traysData[id];
    tray[prop] = value;

    fs.writeFile('./server/data/trays.json', JSON.stringify(traysData), err => {
        console.log('writefile err', err);
    });
}

// EXPORTS //
module.exports.addSeed = addSeed;
module.exports.addTray = addTray;
module.exports.deleteTray = deleteTray;
module.exports.getSeeds = getSeeds;
module.exports.getTasks = getTasks;
module.exports.getTrays = getTrays;