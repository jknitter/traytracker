const express = require('express'),
    router = express.Router();

const dbCtlr = require('../controllers/db.ctlr');

router.get('/seeds', function(req, res){
    res.json(dbCtlr.getSeeds());
});

router.post('/seeds', function(req, res){
    dbCtlr.addSeed(req.body)
        .then(() => res.sendStatus(200))
        .catch(err => res.status(400).json({msg: err}));
});



router.get('/trays', function(req, res){
    res.json(dbCtlr.getTrays());
});

router.post('/trays', function(req, res){
    dbCtlr.addTray(req.body)
        .then(() => res.sendStatus(200))
        .catch(err => res.status(400).json({msg: err}));
});

router.delete('/trays/:id', function(req, res){
    dbCtlr.deleteTray(req.params.id);
    res.send(req.body);
});


router.get('/tasks', function(req, res){
    const json = dbCtlr.getTasks();
    res.json(json);
});

router.get('/tasks/tray/:id/water', function(req, res){
    dbCtlr.updateTray(id, 'watered', Date.now());
    res.send(req.body);
});

module.exports = router;