const express = require('express'),
    bodyParser = require('body-parser');

const app = express();
const port = 3000;

// parse application/json
app.use(bodyParser.json())

app.get('/', (request, res) => {
    res.sendFile('index.html', {root: './app'});
});

app.use(express.static('./app'));
app.use('/api/', require('./routes/api'));

app.listen(port, err => {
    if(err)
        return console.log('something bad happened', err);

    console.log(`server is listening on ${port}`);
})