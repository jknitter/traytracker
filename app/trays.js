Tracker.trays = (function () {
    const addTraySelector = '.addTray',
        trayViewSelector = '.traysView';

    async function addTray(){
        const msgEl = document.querySelector(addTraySelector + ' .msg');
        // save button
        msgEl.nextElementSibling.disabled = true;

        // set up server call
        const opts = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'post',
            body: JSON.stringify(Tracker.form.getFormData(addTraySelector))
        };

        // do post
        const resp = await fetch('/api/trays', opts);
        if(resp.ok)
            Tracker.form.saveOK(addTraySelector);
        else{
            const json = await resp.json()
            Tracker.form.saveErr(addTraySelector, json.msg);
        }
    }

    function dayToMs(day) {
        return day * 1000 * 60 * 60 * 24;
    }
    function msToDays(ms) {
        return ms / 24 / 60 / 60 / 1000;
    }

    function editTray(trayId) {
        console.log(`edit ${trayId}`);
    }

    function getTrayStage(tray) {
        msToDays(0);
        const timePassed = Date.now() - tray.startDate,
            germTime = dayToMs(tray.seed.germination), // in millisec
            growTime = dayToMs(tray.seed.grow), //in millisec
            soakTime = dayToMs(tray.seed.presoak),

            soakStage = timePassed <= soakTime,
            germStage = !soakStage && timePassed <= (soakTime + germTime),
            growStage = !soakStage && !germStage && (timePassed < (soakTime + germTime + growTime)),
            harvestStage = timePassed > (soakTime + germTime + growTime);

        let darkEl = document.createElement('span'),
            growEl = document.createElement('span'),
            harvestEl = document.createElement('span');

        if(soakStage){
            darkEl.textContent = 'Covered';
            darkEl.style.color = 'darkgoldenrod';
            darkEl.style.fontSize = 'larger';
            darkEl.style.fontWeight = 'bold';

            growEl.textContent = '  (move to light on ' + moment(tray.startDate + germTime).format('MMM Do') + ')';

            // temp parent node so we can get the innerHTML
            const el = document.createElement('div');
            el.appendChild(darkEl);
            el.appendChild(growEl);
            return el.innerHTML;
        }
        else if(germStage){
            darkEl.textContent = 'Covered';
            darkEl.style.color = 'darkgoldenrod';
            darkEl.style.fontSize = 'larger';
            darkEl.style.fontWeight = 'bold';

            growEl.textContent = '  (move to light on ' + moment(tray.startDate + germTime).format('MMM Do') + ')';

            // temp parent node so we can get the innerHTML
            const el = document.createElement('div');
            el.appendChild(darkEl);
            el.appendChild(growEl);
            return el.innerHTML;
        }
        else if(growStage){
            growEl.textContent = 'Light';
            growEl.style.color = 'green';
            growEl.style.fontSize = 'larger';
            growEl.style.fontWeight = 'bold';

            harvestEl.textContent = '  (harvest begins on ' + moment(tray.startDate + germTime + growTime).format('MMM Do') + ')';

            // temp parent node so we can get the innerHTML
            const el = document.createElement('div');
            el.appendChild(growEl)
            el.appendChild(harvestEl);
            return el.innerHTML;
        }
        else if(harvestStage){
            harvestEl.textContent = 'Ready for Harvest';
            harvestEl.style.color = 'red';
            harvestEl.style.fontSize = 'larger';
            harvestEl.style.fontWeight = 'bold';

            // temp parent node so we can get the innerHTML
            const el = document.createElement('div');
            el.appendChild(harvestEl);
            return el.innerHTML;
        }
        else {
            console.log('getTrayStage err', err);
        }
    }

    function getTrays() {
        fetch('/api/trays', { method: 'GET' })
            .then(resp => resp.json())
            .then(data => {
                // Cache of the template
                var template = document.getElementById("template-tray").innerHTML;
                // Final HTML variable as empty string
                var listHtml = "";
                // Loop through data, replace placeholder tags
                // with actual data, and generate final HTML
                Object.values(data).forEach(tray => {
                    listHtml += template
                        .replace(/{{id}}/g, tray.id)
                        .replace(/{{name}}/g, tray.name)
                        .replace(/{{seedName}}/g, tray.seed.name)
                        .replace(/{{currentDay}}/g, (((((Date.now() - tray.startDate) / 1000) / 60) / 60) / 24).toPrecision(2))
                        .replace(/{{stage}}/g, getTrayStage(tray))
                        .replace(/{{tray.id}}/g, tray.id);
                });

                // insert filled templates
                document.querySelector(trayViewSelector + ' .items').innerHTML = listHtml;
            });
    }

    function removeTray(trayId) {
        fetch(`/api/trays/${trayId}`, { method: 'DELETE' })
            .then(data => {
                // remove from ui
                document.querySelector('#tray' + trayId).remove();
            })
            .catch(err => console.log('remove tray, ' + err))
    }

    return {
        addTray,
        editTray,
        getTrays,
        getTrayStage,
        removeTray
    }
})();