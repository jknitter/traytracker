Tracker.tasks = (function(){
    function getTasks(params){
       fetch('/api/tasks', {method: 'GET'})
       .then(resp => resp.json())
        .then(list => {
           console.log(list);
       })
       .catch(err => console.log('tasks', err)); 
    }

    return {
        getTasks
    };
})();