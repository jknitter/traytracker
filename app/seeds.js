Tracker.seeds = (function(){
    const addSeedSelector = '.addSeed';

    
    async function addSeed(){
        const msgEl = document.querySelector(addSeedSelector + ' .msg');
        // save button
        msgEl.nextElementSibling.disabled = true;
        
        let data = Tracker.form.getFormData(addSeedSelector);
        // set the checkboxes
        data.stack = data.stack === 'on';

        // set up server call
        const opts = { 
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'post',
            body: JSON.stringify( data )
        };

        // do post
        const resp = await fetch('/api/seeds', opts);
        if(resp.ok)
            Tracker.form.saveOK(addSeedSelector);
        else{
            const json = await resp.json()
            Tracker.form.saveErr(addSeedSelector, json.msg);
        }
    }

    function getSeeds(){
        var opts = {
            method: 'GET',
            headers: {}
        };

        fetch('/api/seeds', opts)
        .then(resp => resp.json())
        .then(data => {
            // add the seed to the dropdown
            const sel = document.querySelector('select#seed');
            sel.options.length = 0;
            Object.values(data).forEach(opt => {
                var option = document.createElement("option");
                option.text = opt.name;
                option.value = opt.id;
                sel.add(option);
            });
        });
    }

    return {addSeed, getSeeds};
})();