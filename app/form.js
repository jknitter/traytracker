Tracker.form = (function (){
    // pass page class name
    function clearForm(selector){
        const form = document.querySelector(selector + ' form');
        form.reset();

        const msgEl = document.querySelector(selector + ' .msg');
        msgEl.classList.add('hide');
        msgEl.classList.remove(['err', 'ok']);
        msgEl.innerHTML = '';

        // enable save
        msgEl.nextElementSibling.disabled = false;
    }

    function getFormData(selector){
        const form = document.querySelector(selector + ' form');
        const formData = new FormData(form);

        // get form data
        let object = {};
        formData.forEach((value, key) => {
            if(typeof(value) === 'string' && !isNaN(value))
                value = parseInt(value);

            object[key] = value;
        });

        return object;
    }

    function saveErr(selector, err){
        const msgEl = document.querySelector(selector + ' .msg');
        msgEl.classList.remove('hide');
        msgEl.classList.add('err');
        msgEl.innerHTML = err;

        // enable save
        msgEl.nextElementSibling.disabled = false;
    }

    function saveOK(selector){
        const msgEl = document.querySelector(selector + ' .msg');
        // success msg
        msgEl.classList.remove('hide');
        msgEl.classList.add('ok');
        msgEl.innerHTML = 'Save Sucess';

        // enable save
        msgEl.nextElementSibling.disabled = false;
    }

    return {
        clearForm,
        getFormData,
        saveErr,
        saveOK
    }
})();