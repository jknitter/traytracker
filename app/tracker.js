let Tracker = (function(){
    function setView(name) {
        const childs = document.querySelector('.content').children;
        for (let i = 0; i < childs.length; i++) {
            childs.item(i).classList.add('hide');
        }

        if(name === "addTray"){
            Tracker.seeds.getSeeds();
            // clear the form
            form = document.querySelector('.addTray' + ' form').reset();
        }
        else if(name === "traysView")
            Tracker.trays.getTrays();

        else if(name === "tasksView")
            Tracker.tasks.getTasks();

        else if(name === 'addSeed')
            // clear the form
            document.querySelector('.addSeed form').reset();
    

        const el = document.querySelector('.' + name);
        if (el.classList.contains('hide'))
            el.classList.remove('hide');
    }

    return {
        setView
    };
})();